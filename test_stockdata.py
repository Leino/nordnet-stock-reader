import stockdata
import os


def test_symbolloader():
    """Test that the value of a key value pair in the loaded json is not empty"""
    assert len(stockdata.symbolloader(stockdata.path_to_stocklist)[0]["Symbol"]) != 0


def test_request_maker():
    requestobject = stockdata.request_maker(29983)
    assert requestobject.status_code == 200

# TODO
def test_save_all_market_data():
    pass


def test_market_data_saver():
    stockdata.marketdata_saver('TEST', '{"1":"123", "2":"132","3":"132"}')
    with open("TEST.json") as file:
        nda_marketdata = stockdata.json.load(file)
    assert len(nda_marketdata) > 1
    os.remove('TEST.json')  # clean up after test
