import requests
import json
import datetime
import random
from time import sleep

path_to_stocklist = "Stocks.json"


def symbolloader(path_to_stocklist):

    with open(path_to_stocklist, "r") as file:
        symbollist = json.load(file)
    return(symbollist)


symbollist = symbolloader(path_to_stocklist)


def request_maker(bookingid):

    headers = {
        'Host': 'www.nordnet.fi',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Language': 'en-US,en;q=0.5',
        'Accept-Encoding': 'gzip, deflate, br',
        'DNT': '1',
        # 'Connection': 'keep-alive',
        # 'Referer': startpage,
        # 'Cookie': 'NOW=2bfcec7677731e0455bad3aae922dfc4edc19588; TUX-COOKIE=rd192o00000000000000000000ffff0a40c029o80; JSESSIONID=82C62F1CF4C31008B4854E3F4FD4FFD7; webapp-cmse=rd797o00000000000000000000ffff0a576182o80; Humany__clientId=c2d34187-5baa-f181-640e-db60714f3599'
        'Upgrade-Insecure-Requests': '1'
        }

    marketid = '24'  # HEX
    bookingid = bookingid
    todaysdate = datetime.date.today()

    startpage = 'https://www.nordnet.fi/mux/web/marknaden/aktiehemsidan/index.html?identifier={bookingid}&marketid={marketid}'.format(bookingid=bookingid, marketid=marketid)
    marketdataendpoint = 'https://www.nordnet.fi/graph/instrument/{marketid}/{bookingid}?from=1970-01-01&to={todaysdate}&fields=last,open,high,low,volume'.format(bookingid=bookingid, marketid=marketid, todaysdate=todaysdate)

    r = requests.get(startpage, headers=headers)
    r.raise_for_status()

    headers['Referer'] = startpage
    r = requests.get(marketdataendpoint, headers=headers)
    r.raise_for_status()

    return r.json()


def marketdata_saver(symbol_name, symbol_marketdata):
    with open(symbol_name + '.json', 'w') as file:
        json.dump(symbol_marketdata, file)


def save_all_market_data(symbollist):
    counter = 0
    for individual in symbollist:
        counter = counter + 1

        print('Getting {nr} of {length}'.format(nr=counter, length=len(symbollist)))
        name_of_symbol = individual['Symbol']
        bookingid_of_symbol = individual['Booking ID'][3:]  # store booking id, remove 'HEX'

        marketdata_for_symbol = request_maker(bookingid_of_symbol)

        marketdata_saver(name_of_symbol, marketdata_for_symbol)
        sleep(random.uniform(4, 7))  # wait between requests to appear as more normal traffic
        print(chr(27) + "[2J")
    print("Fetch complete!")


save_all_market_data(symbollist)
